package util;

import java.awt.*;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.SwingConstants;

/**
 *
 * @author Thomas
 */
public class OkCancel extends javax.swing.JDialog {

    //Variable declaration
    private static JButton cancelButton;
    private static JButton okButton;
    private JLabel textLabel;

    // A return status code - returned if Cancel button has been pressed
    public static final int RET_CANCEL = 0;
    private static int returnStatus = RET_CANCEL;
    // A return status code - returned if OK button has been pressed
    public static final int RET_OK = 1;

    /** 
     * Creates new form OkCancel
     */
    public OkCancel(Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * Create a new dialog with specific message and title.
     * @param title The title of the GUI
     * @param message The message to be displayed.
     */
    public static void newOkCancel(String title, String message) {
        OkCancel okCan = new OkCancel(new javax.swing.JFrame(), true);
        okCan.setTitle(title);
        okCan.textLabel.setText("<HTML>" + message + "</HTML>");
        okCan.setVisible(true);
    }

    /**
     * To be used if the Ok and Cancel buttons are to be changed
     * @param title The title of the GUI
     * @param message The message to displayed the user
     * @param ok The new text of the Ok button
     * @param cancel The new text of the cancel button
     */
    public static void newOkCancel(String title, String message, String ok, String cancel) {
        OkCancel okCan = new OkCancel(new JFrame(), true);
        okCan.setTitle(title);
        okCan.textLabel.setText("<HTML>" + message + "</HTML>");
        changeButtonText(ok, cancel);
        okCan.setVisible(true);
    }

    /** @return the return status of this dialog - one of RET_OK or RET_CANCEL */
    public static int getReturnStatus() {
        return returnStatus;
    }

    /**
     * Initiate the buttons and feel of the GUI
     */
    private void initComponents() {

        okButton = new JButton();
        cancelButton = new JButton();
        textLabel = new JLabel();

        int width = 188;
        int height = 120;
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screen.width - width) / 2;
        int y = (screen.height - height) / 2;
        setBounds(x, y, width, height);

        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent evt) {
                closeDialog(evt);
            }
        });

        okButton.setText("OK");
        okButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        textLabel.setText("<HTML>"+ "This will contain text that will be displayed on the GUI" + "</HTML>");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);


        layout.setHorizontalGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(textLabel)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(okButton)
                    .addComponent(cancelButton))
            )
        );

        layout.linkSize(SwingConstants.HORIZONTAL, new Component[]{cancelButton, okButton});

        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(textLabel)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(okButton)
                        .addComponent(cancelButton))
                )

        );

        pack();
    }

    /**
     * The user presses ok
     * @param evt
     */
    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {
        doClose(RET_OK);
    }

    /**
     * The user presses cancel
     * @param evt
     */
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
        doClose(RET_CANCEL);
    }

    /** 
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {
        doClose(RET_CANCEL);
    }

    /**
     * Dictate what happens when the user closes the GUI.
     * @param retStatus
     */
    private void doClose(int retStatus) {
        returnStatus = retStatus;
        setVisible(false);
        dispose();
    }

    /**
     * Change the text of the buttons
     * @param ok The text to be placed in the ok button
     * @param cancel The text to be placed in the cancel button
     */
    public static void changeButtonText(String ok, String cancel) {
        okButton.setText(ok);
        cancelButton.setText(cancel);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                OkCancel dialog = new OkCancel(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

}
