package util;


import java.awt.*;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Thomas
 */
public class OkDialog extends JDialog {
    private JButton okButton;
    private JLabel textLabel;

    /**
     * Creates new form OkDialog
     */
    public OkDialog(Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * Used by other GUI's to show a new OkDialog box
     * @param title The title of the box
     * @param message The message within the box
     */
    public static void newOkDialog(String title, String message) {
        OkDialog okDia = new OkDialog(new JFrame(), true);
        okDia.setTitle(title);
        okDia.textLabel.setText("<HTML>" + message + "</HTML>"); //HTML tags help wrap the text
        okDia.setVisible(true);
    }

    /**
     * Initiate the buttons and feel of the GUI
     */
    private void initComponents() {

        okButton = new JButton();
        textLabel = new JLabel();

        int width = 260;
        int height = 200;
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screen.width - width) / 2;
        int y = (screen.height - height) / 2;
        setBounds(x, y, width, height);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                closeDialog(evt);
            }
        });

        okButton.setText("OK");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });


        textLabel.setText("<HTML>" + "This is a long message to create a big GUI....... Blah Blah Blah" + "</HTML");

        javax.swing.GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(textLabel)
                    .addComponent(okButton))
            )
        );

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(textLabel)
                .addComponent(okButton))
        );

        pack();
    }

    /**
     * The user presses the ok button
     * @param evt
     */
    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {
        doClose();
    }

    /** 
     * Closes the dialog
     */
    private void closeDialog(WindowEvent evt) {
        doClose();
    }

    /**
     * Dictate what happens if the user closes the form.
     */
    private void doClose() {
        setVisible(false);
        dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        EventQueue.invokeLater(new Runnable() {

            public void run() {
                OkDialog dialog = new OkDialog(new JFrame(), true);
                dialog.addWindowListener(new WindowAdapter() {

                    @Override
                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    //Variable declaration

    // End of variables declaration
}