package util;

import java.text.DecimalFormat;

/**
 * Easily format a number
 * @author Thomas Shippey
 */
public class NumberFormatter {

    /**
     * Get the formatted number in string format
     * @param n The number to be formatted
     * @return The formatted number
     */
    public static String formatDouble(Double n) {
        DecimalFormat number = new DecimalFormat("#0.00");
        return number.format(n);
    }

    /**
     * Get the number to display no decimals.
     * @param n The number
     * @return The formatted number.
     */
    public static String noDecimal(double n) {
        DecimalFormat number = new DecimalFormat("#0");
        return number.format(n);
    }



}

