

import BugTrackerConnection.BugTrackerConnection;
import RepoConnection.RepoConnection;
import SZZ.SZZ;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.GroupLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import sbfgui.buglink.BugLinkPanel;
import sbfgui.bugs.BugPanel;
import sbfgui.insertion.InsertionPanel;
import sbfgui.transaction.TransactionPanel;

/**
 * The house class controls the Welcome GUI option panel. Selecting the right
 * panel to show depending on the tab selected.
 * @author Thomas Shippey 10244080
 */
public class ShipBugFinderControl {

    //The different tab panel displays
    private TransactionPanel trans; //The Transaction Panel Display
    private BugPanel bugs; //The Bug Panel display
    private BugLinkPanel bugLinks; //The Bug Link panel display
    private InsertionPanel insertion; //The Insertion panel display
    private File currentSaveFile; //The current save file being used
    private SZZ szz; //the szz workings
    private String program;

    /**
     * Creates a new Shippey Bug Finder and initialises the panel displays.
     */
    public ShipBugFinderControl() {
        this.szz = new SZZ("","","");
        trans = new TransactionPanel(szz);
        bugs = new BugPanel(szz);
        bugLinks = new BugLinkPanel(szz);
        insertion = new InsertionPanel();
        program = "...";
    }
    
    /**
     * Save the current Shippey Bug Finder with a new file name
     * @param file The file where the instance will be saved
     */
    public void saveAs(File file) {
        ObjectOutputStream oos = null;
        //try {
            currentSaveFile = file;
            szz.save(file);
            /*oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(szz);
            oos.flush();
            oos.close();
        } catch (IOException ex) {
            Logger.getLogger(ShipBugFinderControl.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
    }
    
    /**
     * Save the current instance of Shippey Bug Finder
     */
    public void save(){
        this.saveAs(currentSaveFile);
    }
    
    /**
     * Load previously saved instance
     * @param file The file where the instance is saved
     */
    public void load(File file){
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(new FileInputStream(file));
            currentSaveFile = file;
            szz.load(file);
            setSZZonPanels();
            this.program = szz.getProgram();
            ois.close();
        } //catch (ClassNotFoundException ex) {
            //Logger.getLogger(ShipBugFinderControl.class.getName()).log(Level.SEVERE, null, ex);
        //}
    catch (IOException ex) {
            Logger.getLogger(ShipBugFinderControl.class.getName()).log(Level.SEVERE, null, ex);
        }
        setSZZonPanels();
    }
    
    /**
     * Start a new 
     * @param program The name of the program being used
     * @param bug The location of the bug infomation. Prefix is the type
     *  There is a choice of:
     *  "web:"+location or 
     *  "csv:"+location
     * @param trans The location of the trans information. Prefix is the type
     *  Choice of:
     * "local:"+location
     * "csv:"+location
     * "web:"+location
     */
    public void newBugFinder(String program, String bug, String trans){
        szz = new SZZ(program, trans, bug);
        this.setSZZonPanels();
        this.program = program;
        //getBugLinks(szz.getRepository(), szz.getBugFile(), null);
    }
    
    public void getTransactions(String repository){
        szz.getTrans().parseTransactions(repository);
    }
    
    public void getBugs(String bugLoc, HashMap<String, String> searchParams){
        szz.getBugs().parseBugs(bugLoc, searchParams);
    }
    
    public void getBugLinks(String repository, String bugLoc, HashMap<String, String> searchParams){
        getTransactions(repository);
        getBugs(bugLoc, searchParams);
        szz.findLinks();
    }

    /**
     *
     * @param panel The panel from the WelcomeGUI
     * @param type Which button has been pressed
     * 1 = Transaction
     * 2 = Bugs
     * 3 = Bug Links
     * 4 = Bug Insertion
     * @return The appropriate panel display
     */
    public GroupLayout getContentPanel(JPanel panel, int type) {
        GroupLayout layout = new GroupLayout(panel);
        if(type == 1) {
            layout = trans.setPanelContent(panel);
        }
        else if(type == 2) {            
            layout = bugs.setPanelContent(panel);
        }
        else if (type == 3) {
            layout = bugLinks.setPanelContent(panel);
        }
        else if (type == 4) {
            layout = insertion.setPanelContent(panel);
        }
        
        return layout;
    }
    
    /**
     * Returns the current SZZ instance
     * @return The current szz instance 
     */
    public SZZ getSZZ(){
        return szz;
    }

    public boolean noSaveFile() {
        boolean result = false;
        if(currentSaveFile == null){
            result = true;
        }
        return result;
    }
    
    public String getProgram(){
        String result = "...";
        if(szz != null){
            result = szz.getProgram();
        }
        return program;
    }

    private void setSZZonPanels() {
        trans.setSZZ(szz);
        bugs.setSZZ(szz);
        bugLinks.setSZZ(szz);
    }



}
