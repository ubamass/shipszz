package Clock;


import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import util.OkDialog;


/**
 * Set the timer. In this case it is the lights on the home Automation system.
 * @author Thomas Shippey 10244080
 */
public class TimerSettings extends JFrame {

    //Create the components of the GUI
    private JLabel title, onOffLabel, timeLabel, timeSetLabel;
    private JTable table;
    private JTextField name, timeH, timeM, timeS;
    private JButton back, set;
    private Time clock;
    public static int timerSet; //Whether the timer has been set or not. 1 = yes, 0 = no.
    private static TimerSettings theInstance;
    private ArrayList<String> theLights = new ArrayList<String>(); //The lights to be turned on or off.
    private boolean lightOnOff; //Whether the lights are to be turned on or off
    private JRadioButton on, off;


    /**
     * The singleton method. You do not want more than one timer being created
     * and used.
     * @return The Timer Settings instance.
     */
    public static TimerSettings getInstance() {
        if (theInstance == null) {
            theInstance = new TimerSettings();
        }
        return theInstance;
    }

    /**
     * Create a new instance. The timerSet is set to 0 or off.
     */
    private TimerSettings() {
        initComponents();
        timerSet = 0;
    }

    /**
     * Get if the timer has been set
     * @return 1 if yes, 0 if no.
     */
    public int getTimerSet() {
        return timerSet;
    }

    /**
     * Set the timer to on or off.
     * @param onOff 1 for on, 2 for off.
     */
    public void setTimerSet(int onOff) {
        timerSet = onOff;
    }

    /**
     * Initiate the components of the GUI.
     */
    public void initComponents() {

        //Create the components for the GUI
        title = new JLabel("Title goes here");
        timeLabel = new JLabel("Time:");
        onOffLabel = new JLabel("Turn the light(s) on or off");
        timeSetLabel = new JLabel("Set the time for the timer");
        table = new JTable();
        name = new JTextField("Name of alarm");
        timeH = new JTextField("hh");
        timeM = new JTextField("mm");
        timeS = new JTextField("ss");
        back = new JButton("Back");
        set = new JButton("Set");
        on = new JRadioButton("On");
        off = new JRadioButton("Off");

        //Group the radio buttons.
        ButtonGroup onOff = new ButtonGroup();
        onOff.add(on);
        onOff.add(off);

        //Set the On radio button to on.
        on.setSelected(true);

        table.setModel(new DefaultTableModel(
            new Object [] [] {

            },
            new String [] {
            "Name", "Room", "Set", "Dim Level"
            }
        ) { //Set the column types
            Class[] types = new Class [] {
                String.class, String.class, Boolean.class, String.class
            };
            //Set of the cells are editable
            boolean[] canEdit = new boolean[] {
                false, false, true, false
            };

            /**
             * @return The column class
             */
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            /**
             * @return If the cell is editable or not.
             */
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        //Put the table in a scroll pane
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(table);
        table.setOpaque(true);

        //Set the font for title
        title.setFont(new Font("Tahoma", 1, 11)); //bold

        //Create the actionListeners for the two buttons
        set.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                setActionPerformed(evt);
            }
        });


        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                backActionPerformed(evt);
            }
        });

        //Create the layout for the panel
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(title)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(on)
                        .addComponent(off))
                    .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 410, GroupLayout.PREFERRED_SIZE)
                    .addComponent(timeSetLabel)
                    .addComponent(onOffLabel)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(timeLabel)
                        .addComponent(timeH)
                        .addComponent(timeM)
                        .addComponent(timeS))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(back, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)
                        .addComponent(set, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE))
                    )
        );

        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(title)
                    .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
                    .addComponent(timeSetLabel)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(timeLabel)
                        .addComponent(timeH)
                        .addComponent(timeM)
                        .addComponent(timeS))
                    .addComponent(onOffLabel)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(on)
                        .addComponent(off))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(back)
                        .addComponent(set))
                )
        );




        pack();
    }

    /**
     * What happens when the set button is pushed.
     * The timer is set to 1 - on.
     * The timer time is set.
     * The window is closed.
     * @param evt What is happening.
     */
    private void setActionPerformed(ActionEvent evt) {
        //setTimerLights(on.isSelected());
        int h = numberValid(timeH.getText(), 0, 23);
        int m = numberValid(timeM.getText(), 0, 59);
        int s = numberValid(timeS.getText(), 0, 59);
        clock.setTime(h,m,s);
        timerSet = 1;
        this.dispose();
    }

    /**
     * What happens when the back button is pressed. The actions undertaken by
     * the user on this GUI have no effect to the overall programme.
     * @param evt
     */
    private void backActionPerformed(ActionEvent evt) {
        this.dispose();
    }

    /**
     * Set the title of the table.
     * @param desiredTitle The title text
     */
    public void setTableTitle(String desiredTitle) {
        title.setText(desiredTitle);
    }

    /**
     * Run the GUI
     * @param args
     */
    public static void main(String args[]) {
        EventQueue.invokeLater(new Runnable() {

            public void run() {
                new TimerSettings().setVisible(true);
            }
        });
    }

    /**
     * Set the time of the clock.
     * @param clock The time.
     */
    private void setClock(Time clock) {
        this.clock = clock;
    }

    /**
     * Get the time on the clock
     * @return The time of the clock.
     */
    public Time getClock() {
        return clock;
    }

    /**
     * Get the lights that are to be turned on or off.
     * @return The lights.
     */
    public ArrayList<String> getTheLights() {
        return theLights;
    }

    /**
     * Get whether the lights are to be turned on or off at the timer.
     * @return True if on, False if off.
     */
    public boolean getLightOnOff() {
        return lightOnOff;
    }

    /**
     * Check whether the number that the user has entered is valid. If it is it
     * will return that number in integer form.
     * @param number The number to be checked.
     * @param min The minimum the number can be
     * @param max The maximum the number can be
     * @return The number
     */
    private int numberValid(String number, int min, int max) {
        int newNumber = Integer.parseInt(number);
        if(newNumber > max || newNumber < min) {
            OkDialog.newOkDialog("Illegal number",
                "Number " + newNumber + " out of range. Must be in the range " 
                    + min + " to " + max);
            throw new IllegalArgumentException(

                "Number " + newNumber + " out of range. Must be in the range "
                    + min + " to " + max);
        }
        else {
            return newNumber;
        }
    }

}



