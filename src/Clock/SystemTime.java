package Clock;

import java.util.Calendar;

/**
 *
 * @author Thomas Shippey 10244080
 * Allows the easy access of the current time and date.
 */
public class SystemTime {

    /**
     * Returns the relevant time fragment you want.
     * @param t The time you want
     * hh = Hour
     * mm = Minute
     * ss = Second
     * ms = Millisecond
     * @return The relevant time fragment.
     */
    public static int getTime(String t) {
        Calendar now = Calendar.getInstance();
        if (t.equals("hh")) {
        return now.get(Calendar.HOUR_OF_DAY);
        }
        else if (t.equals("mm")) {
            return now.get(Calendar.MINUTE);
        }
        else if (t.equals("ss")) {
            return now.get(Calendar.SECOND);
        }
        else if (t.equals("ms")) {
            return now.get(Calendar.MILLISECOND);
        }
        return 0;
    }

    /**
     * Get the current day of the week
     * @return The day of the week in long form.
     */
    public static String getDay() {
        String retDay = "Tuesday";
        Calendar now = Calendar.getInstance();
        int day = now.get(Calendar.DAY_OF_WEEK);
        switch(day) {
            case 1: retDay = "Sunday";
            break;
            case 2: retDay = "Monday";
            break;
            case 3: retDay = "Tuesday";
            break;
            case 4: retDay = "Wednesday";
            break;
            case 5: retDay = "Thursday";
            break;
            case 6: retDay = "Friday";
            break;
            case 7: retDay = "Saturday";
            break;

        }
        return retDay;
    }

    /**
     * Get the month in text form
     * @param monthNum The month number
     * @return The month in text form.
     */
    public static String getMonth(int monthNum) {
        String retMonth = null;
        int month = monthNum - 1;
        switch(month) {
            case 0: retMonth = "January";
            break;
            case 1: retMonth = "February";
            break;
            case 2: retMonth = "March";
            break;
            case 3: retMonth = "April";
            break;
            case 4: retMonth = "May";
            break;
            case 5: retMonth = "June";
            break;
            case 6: retMonth = "July";
            break;
            case 7: retMonth = "August";
            break;
            case 8: retMonth = "September";
            break;
            case 9: retMonth = "Ocotber";
            break;
            case 10: retMonth = "November";
            break;
            case 11: retMonth = "December";
            break;
        }
        return retMonth;
    }

    /**
     * Get the current year
     * @return The current year
     */
    public static int getYear() {
        Calendar now = Calendar.getInstance();
        return now.get(Calendar.YEAR);
    }

    /**
     * Get the current day of the month
     * @return The current day of the month
     */
    public static  int getDayOfMonth() {
        Calendar now = Calendar.getInstance();
        return now.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Get the current month of the year
     * @return The current month of the year
     */
    public static int getNumMonth() {
        Calendar now = Calendar.getInstance();
        return now.get(Calendar.MONTH) + 1;
    }




}




