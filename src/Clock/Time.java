package Clock;

/**
 *
 * @author David Bowes
 */


public class Time {
Digit h,m,s, day, month, year;
TicToc tt;

    public Time () {
        h=new Digit();
        h.setMax(23);
        m=new Digit();
        m.setMax(59);
        s=new Digit();
        s.setMax(59);
        s.setNextDigit(m);
        m.setNextDigit(h);
        day = new Digit();
        day.setMax(31);
        day.setMin(1);
        month = new Digit();
        month.setMax(12);
        month.setMin(1);
        year = new Digit();
        h.setNextDigit(day);
        day.setNextDigit(month);
        month.setNextDigit(year);
        tt = TicToc.getInstance();
        tt.addListener(s);
    }


    public void setTime (int h, int m, int s) {
        this.h.setValue(h);
        this.m.setValue(m);
        this.s.setValue(s);
    }

    public void setDate(int day, int month, int year) {
        this.day.setValue(day);
        this.month.setValue(month);
        this.year.setValue(year);
    }

    public String getTime() {
        return h.getValue()+":"+m.getValue()+":"+s.getValue();
    }

    String getDate() {
        return day.getValue() + "/" + month.getValue() + "/" + year.getValue();
    }

    public boolean equals(Object o)
    {
        Time t=(Time) o;
        return (t.h.getValue().equals(h.getValue()) &&
                t.m.getValue().equals(m.getValue()) &&
                t.s.getValue().equals(s.getValue()));
    }

    public String getTime(TimeFormatter tf) {
        return h.getValue(tf)+":"+m.getValue(tf)+":"+s.getValue(tf);
    }

    String getDate(TimeFormatter tf) {
        return day.getValue(tf) + "/" + month.getValue(tf) + "/" + year.getValue(tf);
    }

    void setDayMax(int max) {
        day.setMax(max);
    }

    Digit getDigit(String time) {
        Digit d = null;
        if(time.equals("hh")) {
            d = h;
        }
        if(time.equals("mm")) {
            d = m;
        }
        if(time.equals("ss")) {
            d = s;
        }
        return d;
    }
}

