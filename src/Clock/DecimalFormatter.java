package Clock;

/**
 *
 * @author David Bowes 2010
 * Taken from his SDTM lectures
 * Formats the time
 */
public class DecimalFormatter implements TimeFormatter{

    /**
     * Adds a preceeding zero if the number is less that two digits.
     * @param n The number
     * @return The formatted number
     */
    public String formatInt(int n) {
        String result=""+n;
        while(result.length()<2)
        {
            result="0"+result;
        }
        return result;
    }

}
