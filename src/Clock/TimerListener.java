package Clock;

/**
 * A Timer Listener
 * @author Thomas Shippey
 */
public interface TimerListener {

    public void set();

}
