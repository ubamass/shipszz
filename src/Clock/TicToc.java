package Clock;

/**
 * David Bowes - 2010 - Software Development Tools and Methods Practical and
 * Seminar
 */
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.CECA41BA-0F50-BD99-E478-575A0692C6E6]
// </editor-fold> 
public class TicToc extends Thread {

    private static TicToc theInstance = null;
    private ArrayList<TicTocListener> listeners = new ArrayList<TicTocListener>();
    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.D1924800-F7C4-845D-F371-9ED22FA071FC]
    // </editor-fold> 

    private TicToc() {
        this.start();
    }

    public static TicToc getInstance() {
        if (theInstance == null) {
            theInstance = new TicToc();
        }
        return theInstance;
    }


    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(TicToc.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (TicTocListener ttl : listeners) {
                ttl.increment();
            }
        }
    }

    public void addListener(TicTocListener ttl) {
        listeners.add(ttl);
    }
}

