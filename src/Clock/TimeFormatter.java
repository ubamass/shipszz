package Clock;

/**
 *
 * @author David Bowes 2010. Taken from his lectures in SDTM
 */
public interface TimeFormatter {

    String formatInt(int n);
}
