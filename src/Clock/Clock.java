package Clock;

/**
 *
 * @author David Bowes 2010.
 * Taken from his lectures during Software development tools and methods.
 * I have added the date and timer functions.
 */

public class Clock {

    Time t;
    Timer timer;

    public Clock () {
        t=new Time();
        timer = new Timer();
        t.tt.addListener(timer);
        timer.setTime(t);
    }

    /**
     * Get the time
     * @param tf The format you want the time in
     * @return The time.
     */
    public String getTime (TimeFormatter tf) {
        return t.getTime(tf);
    }

    /**
     * Get the date
     * @param tf What format you want the date in.
     * @return The date
     */
    public String getDate(TimeFormatter tf) {
        return t.getDate(tf);
    }

    /**
     * Set the time
     * @param h The hour
     * @param m The minute
     * @param s The seconds.
     */
    public void setTime (int h, int m, int s) {
        t.setTime(h,m,s);
    }

    /**
     * Set the maximum number of days the month can have.
     */
    public void setDayMax() {
        int month = t.month.value;
                switch(month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                t.setDayMax(31);
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                t.setDayMax(30);
                break;
            case 2:
                if((t.year.value % 400 == 0) || ((t.year.value % 4 == 0) && (t.year.value % 100 != 0))) {
                    t.setDayMax(29);
                }
                else {
                    t.setDayMax(28);
                }
                break;

        }
    }

    /**
     * Set the date
     * @param day The day of the month
     * @param month The month of the year
     * @param year The year
     */
    public void setDate(int day, int month, int year) {
        t.setDate(day, month, year);
    }

    /**
     * Set the timer time.
     * @param h The hour
     * @param m The minute
     * @param s The second
     */
    public void setTimer(int h, int m, int s) {

    }

    /**
     * Get the timer
     * @return The timer
     */
    public Timer getTimer() {
        return timer;
    }

    /**
     * @return The TicToc
     */
    public TicToc getTicToc()
    {
        return t.tt;
    }

}

