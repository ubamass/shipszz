package Clock;

/**
 * A timer
 * @author Thomas Shippey 10244080
 */
public class Timer extends Time implements TicTocListener {
    
    private Time timer; //The time of the timer
    private boolean timerSet; //Check if the timer has been set
    private Time time; //The current time.
    private TimerListener listener; //In order to listen to the timer listener.

    /**
     * Creates a new Timer. At creation the timer is not set. Makes sure that
     * seconds in the timer time does not increase every second.
     */
    public Timer() {
        super();        
        timerSet = false;
        this.s.listen=false; //Make sure the timer time does not increase every second.
    }

    /**
     * Set the timer time.
     * @param t
     */
    public void setTime(Time t) {
        time = t;
    }

    /**
     * Set the timer to on.
     */
    public void setTimer() {
        timerSet = true;
    }

    /**
     * Get the current status of the timer.
     * @return True if on, False if off
     */
    public boolean getSet() {
        return timerSet;
    }

    /**
     * Sets the timer to listen to the timer listner.
     * @param aThis A timer listener
     */
    public void setTimerListener(TimerListener aThis) {
        listener = aThis;
    }

    /**
     * Get the timer time.
     * @return The timer time.
     */
    public Time getTimerTime() {
        return time;
    }

    /**
     * What happens every second.
     */
    public void increment() {
        //Nothing happens every second.
    }

}
