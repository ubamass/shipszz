package Clock;

/**
 *
 * @author David Bowes 2010
 * Taken from his SDTM Lectures
 */

public class Digit implements TicTocListener {
boolean listen=true;
 
    private int max, min;
    int value=0;
    Digit nextDigit;


    public Digit () {
    }


    public int getMax () {
        return max;
    }

    public void setMax (int val) {
        this.max = val;
    }

    public void setMin(int min) {
        this.min = min;
    }


    public void increment () {
        if (listen){
        value=value+1;
        
        if (value>max){
            value=0;
            if (nextDigit!=null) {nextDigit.increment();}
        }

        if(value < min) {
            value = min;
        }
        }
    }

    public void setValue (int a) {
        value=a;
    }

    public void setNextDigit(Digit d)
    {
        nextDigit=d;
    }

    String getValue() {
        return value+"";
    }

    String getValue(TimeFormatter tf) {
        return tf.formatInt(value);
    }

}

