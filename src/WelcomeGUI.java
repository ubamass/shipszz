
import Clock.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.HashMap;
import javax.swing.*;
import javax.swing.JFrame;
import javax.swing.filechooser.FileFilter;

/**
 * The WelcomeGUI is the main program. It controls everything the user is to
 * see. It has an inbuilt clock to show the time and date to the user.
 *
 * @author Thomas Shippey 10244080
 */
public class WelcomeGUI extends JFrame implements ActionListener, TicTocListener {
    //All the relevant peices for the GUI

    private JButton home;
    private JButton trans;
    private JButton bugLinks;
    private JButton bugs;
    private JButton insertion;
    private JLabel time;
    private JLabel date;
    private JLabel title;
    private Clock clock; //A clock that controls the time.
    private JPanel panel; //The panel for the various options.
    TimeFormatter tf = new DecimalFormatter(); //Formats the time
    private ShipBugFinderControl szz; //This will help control the panel.
    private JMenuBar menuBar;
    private JMenu file, about;
    private JMenuItem fileSave, fileOpen, fileNew, fileExit, fileSaveAs, aboutAbout;
    private JLabel programLbl;
    private HashMap<String, String> searchParams;

    /**
     * Create a new Welcome GUI. Initialises the components and then sets the
     * appropiate time. The time will be the time on the computer system.
     */
    public WelcomeGUI() {
        clock = new Clock();
        clock.getTicToc().addListener(this);
        clock.setTime(SystemTime.getTime("hh"), SystemTime.getTime("mm"), SystemTime.getTime("ss"));
        clock.setDate(SystemTime.getDayOfMonth(), SystemTime.getNumMonth(), SystemTime.getYear());
        initComponents();

    }

    /**
     * Initialise all the components of the GUI.
     */
    private void initComponents() {
        date = new JLabel(clock.getDate(tf));
        time = new JLabel(clock.getTime(tf));
        title = new JLabel("Shippey Bug Link Finder | Welcome");
        home = new JButton();
        home.setIcon(new ImageIcon("pictures/home.gif"));
        trans = new JButton("Transactions");
        bugLinks = new JButton("BugLinks");
        bugs = new JButton("Bugs");
        insertion = new JButton("Insertion Points");
        panel = new JPanel();
        Dimension dim = new Dimension();
        dim.setSize(50, 50);
        bugLinks.setPreferredSize(dim);
        Dimension dimPanel = new Dimension(400, 200);
        panel.setPreferredSize(dimPanel);
        panel.setBorder(BorderFactory.createEtchedBorder());
        panel.setOpaque(false);

        createMenuBar();


        //Initiate ShipBugFinderControl
        szz = new ShipBugFinderControl();


        //Make the home icon look like a label, but act as a button
        home.setFocusPainted(false);
        home.setMargin(new Insets(0, 0, 0, 0));
        home.setContentAreaFilled(false);
        home.setBorderPainted(false);
        home.setOpaque(false);

        //Add a home ActionListener
        home.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent evt) {
                homeActionPerformed(evt);
            }
        });

        //Set the panel layout to the welcome screen
        setPanelLayout(0);

        //Add a shoppingList ActionListener
        trans.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent evt) {
                transActionPerformed(evt);
            }
        });

        //Add a doors actionListener
        bugs.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent evt) {
                bugsActionPerformed(evt);
            }
        });

        //Add a lights actionListener
        bugLinks.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent evt) {
                bugLinksActionPerformed(evt);
            }
        });

        //Add a alarm actionListener
        insertion.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent evt) {
                insertionActionPerformed(evt);
            }
        });

        //Set the layout of the GUI
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);


        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(home, GroupLayout.Alignment.CENTER).addComponent(trans).addComponent(bugs).addComponent(bugLinks).addComponent(insertion)).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(title).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 99, Short.MAX_VALUE).addComponent(time).addComponent(date)).addGroup(layout.createSequentialGroup().addGap(6, 6, 6).addComponent(panel))).addContainerGap()));

        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(home).addComponent(title).addComponent(time).addComponent(date)).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(trans).addComponent(bugs).addComponent(bugLinks).addComponent(insertion)).addComponent(panel))));

        //Links the tab buttons.
        layout.linkSize(trans, bugs, bugLinks, insertion);
        trans.setSize(400, 600);

        setTitle("Shippey Bug Link Finder");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();

        //Change the background color of GUI
        Container con = getContentPane();
        con.setBackground(Color.WHITE);

        //Set GUI to centre of screen
        int height = 550;
        int width = 946;
        //Get resolution of users screen
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screen.width - width) / 2;
        int y = (screen.height - height) / 2;
        setBounds(x, y, width, height);

        this.setResizable(false);

    }

    /**
     * Creates the menubar for the GUI
     */
    private void createMenuBar() {
        //Create the menu bar

        menuBar = new JMenuBar(); //the menu bar        

        //Build sub Menu Bars
        file = new JMenu("File");
        file.setMnemonic(KeyEvent.VK_F);
        file.getAccessibleContext().setAccessibleDescription("Create a new, "
                + "open or save the a Bug Finder Instance.");
        about = new JMenu("About");
        about.setMnemonic(KeyEvent.VK_A);
        about.getAccessibleContext().setAccessibleDescription("Find out about Bug Finder");

        //Build File MenuBar Items
        fileSave = new JMenuItem("Save", KeyEvent.VK_S);
        fileSave.getAccessibleContext().setAccessibleDescription("Save current Bug Finder");
        fileSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.ALT_MASK));

        fileSaveAs = new JMenuItem("Save As", KeyEvent.VK_A);
        fileSaveAs.getAccessibleContext().setAccessibleDescription("Save current "
                + "Bug Finder as a certain file");
        fileSaveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.ALT_MASK));

        fileOpen = new JMenuItem("Open", KeyEvent.VK_O);
        fileOpen.getAccessibleContext().setAccessibleDescription("Open Bug Finder");
        fileOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.ALT_MASK));

        fileNew = new JMenuItem("New", KeyEvent.VK_N);
        fileNew.getAccessibleContext().setAccessibleDescription("Create new Bug Finder");
        fileNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.ALT_MASK));

        fileExit = new JMenuItem("Exit", KeyEvent.VK_X);
        fileExit.getAccessibleContext().setAccessibleDescription("Exit Shippey Bug Finder");
        fileExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.ALT_MASK));

        aboutAbout = new JMenuItem("About", KeyEvent.VK_B);
        aboutAbout.getAccessibleContext().setAccessibleDescription("About the Bug Finder");
        aboutAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));

        //Add file MenuBar items to file bar
        file.add(fileSave);
        file.add(fileSaveAs);
        file.add(fileOpen);
        file.add(fileNew);
        file.addSeparator();
        file.add(fileExit);
        about.add(aboutAbout);

        //Add menu bar to JFrame
        menuBar.add(file);
        menuBar.add(about);
        setJMenuBar(menuBar);

        //addactionlisteners
        fileSave.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                fileSaveActionPerformed(e);
            }
        });

        fileSaveAs.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                fileSaveAsActionPerformed(e);
            }
        });

        fileOpen.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                fileOpenActionPerformed(e);
            }
        });

        fileNew.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                fileNewActionPerformed(e);
            }
        });

        fileExit.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                fileExitActionPerformed(e);
            }
        });

        aboutAbout.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                aboutAboutActionPerformed(e);
            }
        });
    }

    /**
     * Runs the GUI
     *
     */
    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                new WelcomeGUI().setVisible(true);

            }
        });
    }

    /**
     * Sets the panel to the right layout depending on the tab button pushed.
     *
     * @param type The button that has been pushed. 0 = Welcome 1 = Transactions
     * 2 = Bugs 3 = Bug Links 4 = Insertions
     */
    private void setPanelLayout(int type) {
        String defaultTitle = "Shippey Bug Finder | ";
        setButtonColour(type);
        String program = szz.getProgram();
        if (type == 1) {
            setTitle(defaultTitle + "Transactions");
            title.setText("Transactions in " + program);
            szz.getContentPanel(panel, type);
        } else if (type == 2) {
            setTitle(defaultTitle + "Bugs");
            title.setText("Bugs in " + program);
            szz.getContentPanel(panel, type);
        } else if (type == 3) {
            setTitle(defaultTitle + "Bug Links");
            title.setText("The Bug Links discovered in " + program);
            szz.getContentPanel(panel, type);

        } else if (type == 4) {
            setTitle(defaultTitle + "Insertion Points");
            title.setText("Insertion Points in " + program);
            szz.getContentPanel(panel, type);
        } else {
            getPanelLayout(panel);
            setTitle(defaultTitle + "Welcome");
            title.setText("Welcome to the Shippey Bug Link Finder");
        }
    }

    /**
     * Sets the button colour to White if it has been selected and the panel is
     * showing its layout.
     *
     * @param type The button that has been pushed. 0 = Welcome 1 = Shopping
     * List 2 = Door Locks 3 = Lights 4 = Alarm
     */
    public void setButtonColour(int type) {
        Color def;
        def = UIManager.getColor("Button.background");
        if (type == 1) {
            trans.setBackground(Color.WHITE);
            trans.setFocusPainted(false);
            bugs.setBackground(def);
            bugLinks.setBackground(def);
            insertion.setBackground(def);
        } else if (type == 2) {
            bugs.setBackground(Color.WHITE);
            bugs.setFocusPainted(false);
            trans.setBackground(def);
            bugLinks.setBackground(def);
            insertion.setBackground(def);

        } else if (type == 3) {
            bugLinks.setBackground(Color.WHITE);
            bugLinks.setFocusPainted(false);
            bugs.setBackground(def);
            trans.setBackground(def);
            insertion.setBackground(def);
        } else if (type == 4) {
            insertion.setBackground(Color.WHITE);
            insertion.setFocusPainted(false);
            bugs.setBackground(def);
            trans.setBackground(def);
            bugLinks.setBackground(def);
        } else {
            bugs.setBackground(def);
            trans.setBackground(def);
            bugLinks.setBackground(def);
            insertion.setBackground(def);
        }
    }

    /**
     * What happens when the transaction button is pushed. The transaction
     * layout is shown on the panel.
     *
     * @param evt The event of the button being pushed
     */
    public void transActionPerformed(ActionEvent evt) {
        panel.removeAll();
        setPanelLayout(1);
    }

    /**
     * What happens when the home icon is clicked upon. The welcome screen is
     * shown
     *
     * @param evt The event of the icon being clicked upon.
     */
    public void homeActionPerformed(ActionEvent evt) {
        panel.removeAll();
        setPanelLayout(0);
    }

    /**
     * What happens when the bugs button is pushed. The bugs layout is shown on
     * the panel.
     *
     * @param evt The event of the button being pushed
     */
    public void bugsActionPerformed(ActionEvent evt) {
        panel.removeAll();
        setPanelLayout(2);
    }

    /**
     * What happens when the bug link button is pushed. The bug link layout is
     * shown on the panel.
     *
     * @param evt The event of the button being pushed
     */
    public void bugLinksActionPerformed(ActionEvent evt) {
        panel.removeAll();
        setPanelLayout(3);
    }

    /**
     * What happens when the insertion button is pushed. The insertion layout is
     * shown on the panel.
     *
     * @param evt The event of the button being pushed
     */
    public void insertionActionPerformed(ActionEvent evt) {
        panel.removeAll();
        setPanelLayout(4);
    }

    /**
     * This happens every second. On this GUI it controls the time.
     */
    public void increment() {
        time.setText(clock.getTime(tf));
        date.setText(clock.getDate(tf));
        clock.setDayMax();
        checkSaveAs();
    }

    /**
     * Not implemented here.
     *
     * @param e The event.
     */
    public void actionPerformed(ActionEvent e) {
        //Not implemented here
    }

    /**
     * Returns the welcome panel layout.
     *
     * @param panel The panel used to show the different options.
     * @return The welcome layout.
     */
    private GroupLayout getPanelLayout(JPanel panel) {
        JLabel welcomeText = new JLabel();
        welcomeText.setText("<HTML>" + "Welcome to the Shippey Bug Link Finder."
                + " Please use the tabs on the left to use the system." + "</HTML>");
        title.setText("Welcome to the Shippey Bug Link Finder");
        GroupLayout panelLayout = new GroupLayout(panel);
        panel.setLayout(panelLayout);

        panelLayout.setHorizontalGroup(
                panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(panelLayout.createSequentialGroup().addGap(21, 21, 21).addComponent(welcomeText).addContainerGap(133, Short.MAX_VALUE)));

        panelLayout.setVerticalGroup(
                panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(panelLayout.createSequentialGroup().addContainerGap().addComponent(welcomeText).addContainerGap(76, Short.MAX_VALUE)));

        return panelLayout;
    }

    private void fileSaveAsActionPerformed(ActionEvent e) {
        JFileChooser saveAs = new JFileChooser("Save As");
        FileFilter onlySZZFiles = new SZZFileFilter();
        saveAs.setCurrentDirectory(new File("/output"));
        saveAs.setFileFilter(onlySZZFiles);
        if (saveAs.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            String saveFilePath = setSavePath(saveAs.getSelectedFile().getPath());
            File saveFile = new File(saveFilePath);
            szz.saveAs(saveFile);
        }
        JOptionPane.showMessageDialog(this, "File Saved");
    }

    private void fileSaveActionPerformed(ActionEvent e) {
        szz.save();
        JOptionPane.showMessageDialog(this, "File Saved");
    }

    private void fileOpenActionPerformed(ActionEvent e) {
        JFileChooser load = new JFileChooser();
        load.setCurrentDirectory(new File("."));
        load.setFileFilter(new SZZFileFilter());
        if (load.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File loadFile = load.getSelectedFile();
            szz.load(loadFile);
        }
        JOptionPane.showMessageDialog(this, "File "+ load.getSelectedFile().getName() + " Loaded");
    }

    private void fileExitActionPerformed(ActionEvent e) {
        dispose();
        System.exit(0);
    }

    private void fileNewActionPerformed(ActionEvent e) {
        StartNewSBF neew = new StartNewSBF(this, true);
        boolean correctInput = false;
        neew.setVisible(true);
        if (neew.getReturnStatus() == 1) { //dont do anything unless return = ok
            while (correctInput == false && neew.getReturnStatus() == 1) {
                String bugInfo = neew.getBugInfo();
                String transInfo = neew.getTransInfo();
                String program = neew.getProgram();
                if ((bugInfo.isEmpty() || transInfo.isEmpty() || program.isEmpty())) {
                    neew.setVisible(true);
                } else {
                    correctInput = true;
                    szz.newBugFinder(program, bugInfo, transInfo);
                    if(bugInfo.startsWith("web")){
                        searchParams = new HashMap<String, String>();
                        searchParams.put("product", "JDT");
                        searchParams.put("component", "core");
                        searchParams.put("version", "3.0");
                    }
                    else {
                        searchParams = null; //not needed if using csv file
                    }
                    szz.getBugLinks(transInfo, bugInfo, searchParams);
                }
            }
            neew.dispose();
            
        }
    }

    private void aboutAboutActionPerformed(ActionEvent e) {
    }

    private void checkSaveAs() {
        if (szz.noSaveFile()) {
            this.fileSave.setEnabled(false);
        } else {
            this.fileSave.setEnabled(true);
        }
    }

    private String setSavePath(String path) {
        String result = path;
        if(!path.endsWith(".sssz")){
            result = path + ".sssz";
        }
        return result;
    }

    private class SZZFileFilter extends FileFilter {

        public boolean accept(File pathname) {
            boolean result = false;
            if (pathname.isDirectory()) {
                result = true;
            } else if (pathname.getName().toLowerCase().endsWith(".sssz")) {
                result = true;
            }
            return result;
        }

        @Override
        public String getDescription() {
            return ".sssz";
        }
    }
}